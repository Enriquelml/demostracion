export const Menu = [
    {
        title: 'Plataforma web',
        msg: 'Creación de páginas para publicidad o algún tipo de control administrativo o e-commerce',
        img: '/src/assets/jpg/webTwo.jpg '
    },
    {
        title: 'Desarrollo de Apps',
        msg:'Creación de aplicaciones para los sistemas operativos iOS y Android',
        img: '/src/assets/jpg/movil.jpg'
    }, 
    {
        title: 'Base de datos',
        msg: 'Estructurar una base de datos para el mejor desempeño del desarrollo web ',
        img: '/src/assets/jpg/bdTwo.jpg'
    }
]